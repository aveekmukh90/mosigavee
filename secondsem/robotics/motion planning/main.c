#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define WIDTH 10
#define HEIGHT 10

int start_i =-1;
int start_j =-1;

int goal_i =-1;
int goal_j =-1;

void displayMatrix();
void initializeMatrix();
void inputMap();
int inputGoal();
int inputStart();
void printError(char *);
void distanceAllot(int i , int j, int valold);

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

int matrix[WIDTH][HEIGHT];

void printError(char *str){
  printf(ANSI_COLOR_RED);
  printf("%s",str);
  printf(ANSI_COLOR_RESET);
}

int main()
{
  initializeMatrix();
  //displayMatrix();

  //Step1:
  inputMap();
  //Step2:
  int ig = 0;
  while(!ig)
  {
    ig=inputGoal();
  }

  //Step3:
  int is = 0;
  while(!is)
  {
    is=inputStart();
  }

  displayMatrix();

  //Step4: graph distance measuring
  distanceAllot(start_i,start_j,0);

  printf("\n\n");
  displayMatrix();
  //Step5: diplay path in colour

  return 0;
}

//I will allocate my neighbours with one more than my number
void distanceAllot(int i , int j, int valold)
{
  if(i<0 || i>WIDTH || j<0 || j>HEIGHT){
    return;
  }
  //top
  i=i-1;
  if(matrix[i][j]==0 && i!=-1){
    matrix[i][j]=valold+1;
  }
  i=i+1;
  //bottom
  i=i+1;
  if(matrix[i][j]==0 && i!=HEIGHT){
    matrix[i][j]=valold+1;
  }
  i=i-1;
  //left
  j=j-1;
  if(matrix[i][j] == 0 && j!=-1){
    matrix[i][j]=valold+1;
  }
  j=j+1;
  //right
  j=j+1;
  if(matrix[i][j] == 0 && j!=WIDTH){
    matrix[i][j]=valold+1;
  }
  j=j-1;

  if(goal_i < start_i && goal_j < start_j){
    //Goal is topleft to start
    distanceAllot(i-1,j,valold+1);
    distanceAllot(i,j-1,valold+1);
  }
  if(goal_i < start_i && goal_j > start_j){
    //Goal is topright to start :p
    distanceAllot(i-1,j,valold+1);
    distanceAllot(i,j+1,valold+1);
  }
  if(goal_i > start_i && goal_j > start_j){
    //Goal is bottomright to start
    distanceAllot(i+1,j,valold+1);
    distanceAllot(i,j+1,valold+1);
  }
  if(goal_i < start_i && goal_j > start_j){
    //Goal is bottomleft to start
    distanceAllot(i+1,j,valold+1);
    distanceAllot(i,j-1,valold+1);
  }
  //distanceAllot(i-1,j,valold+1);
  //distanceAllot(i+1,j,valold+1);
  //distanceAllot(i,j-1,valold+1);
  //distanceAllot(i,j+1,valold+1);
}

void inputMap(){
  int i,j;
  char c;

  while(1){
    displayMatrix();
    printf("Want to input map?\n");
    c=getchar();
    //printf("Choice %c",c);
    if(c=='N' || c=='n'){
      break;
    }
    printf("Enter walled cell row:\n");
    scanf("%d",&i);

    printf("Enter walled cell column:\n");
    scanf("%d",&j);
    fflush(stdin);
    if(i<WIDTH && i>0 && j<HEIGHT && j>0){
      matrix[i-1][j-1]='X';
    }else{
      printError("Invalid input! Index out of range.\n");
    }

    getchar();
  }
}

int inputGoal(){
  int i,j;
  char c;

  printf("Step 2: Enter Goal.\n");

  printf("Enter Goal cell row:\n");
  scanf("%d",&i);

  printf("Enter Goal cell column:\n");
  scanf("%d",&j);
  if(i<=0 || i>WIDTH || j<=0 || j>HEIGHT){
    printError("Invalid input! Index out of range.\n");
    return 0;
  }
  if(matrix[i-1][j-1]=='X'){
    printError("Invalid input! Goal cannot be in Wall.\n");
    return 0;
  }

  goal_i = i-1;
  goal_j = j-1;
  matrix[goal_i][goal_j]='G';
  return 1;
}

int inputStart(){
  int i,j;
  char c;

  printf("Step 2: Enter Start.\n");

  printf("Enter Start cell row:\n");
  scanf("%d",&i);

  printf("Enter Start cell column:\n");
  scanf("%d",&j);
  if(i<=0 || i>WIDTH || j<=0 || j>HEIGHT){
    printError("Invalid input! Index out of range.\n");
    return 0;
  }
  if(matrix[i-1][j-1]=='X'){
    printError("Invalid input! Start cannot be in Wall.\n");
    return 0;
  }

  start_i = i-1;
  start_j = j-1;
  matrix[start_i][start_j]='S';
  return 1;
}

void initializeMatrix(){
  printf("Step initialized!\n");
}

void displayMatrix(){
  int i;
  int j;

  for (i = 0; i<HEIGHT;i++)
  {
    printf("|");
    for(j = 0; j<WIDTH; j++)
    {
      if(matrix[i][j] == 0){
        printf(" 0  |");
      }else{
        if(i== start_i && j== start_j || i== goal_i && j == goal_j || matrix[i][j]=='X'){
          printf(" %c |", matrix[i][j]);
        }else{
          printf(" %d |", matrix[i][j]);
        }
      }

    }
    printf("\n");
    for(j = 0; j<WIDTH; j++)
    {
      printf("-----");
    }
    printf("\t\n");
  }
}
