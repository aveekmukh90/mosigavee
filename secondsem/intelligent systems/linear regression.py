import numpy as np
from numpy import matrix
import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show

xraw= [[145],[154],[158],[161],[172],[158],[163],[169],[156],[161]]
yraw = [[46],[56],[61],[64],[77],[62],[67],[73],[58],[65]]

M = np.mean(yraw)
x = matrix(xraw)
y= matrix(yraw)
#print(x,y)
#Direct method
t1 = (x.T*x).I
t2 = (x.T*y)
w = t1*t2
ytilde = x*w
ytilderaw = np.squeeze(np.asarray(ytilde))

def Lossfunc(max_index, y , ym):
    s = 0
    for i in range(0,max_index):
        s += np.square(y[i][0]-ym[i][0])
    return s/(2*M)

print("Loss Direct=")
print(w,Lossfunc(10,y,ytilde))

#print(w)

#print(ytilderaw)
'''
fig, ax = subplots()
ax.plot(xraw, yraw, 'bo')
ax.plot(xraw, ytilderaw, 'ro')
ax.set_xlabel("height")
ax.set_ylabel("weight")
show()
'''
def sigma(index,y,x):
    s = 0
    for i in range(0,index):
        s+= y[i][0]*x[i][0]
    return s

def GradientDescent(iter,alpha):
    epsilon = 0.000005
    w0 = 0
    w =  0#(y[1][0]-y[0][0])/(x[1][0]-x[0][0])
    lossprev = 10000000;
    #print(w,lossprev)
    for i in range(0,iter):
        loss = Lossfunc(10,y,x*w)
        lossdiff = loss - lossprev
        lossdiff = -lossdiff if (lossdiff <0) else lossdiff
        if(lossdiff < epsilon):
            print("close loss",loss, lossprev)
            return w,i

        print(i,w,loss)
        #print(diff_sig)

        if(loss>lossprev):
            #we have a problem, we went too far!! reduce aplha
            w=wprev
            loss=lossprev
            alpha = alpha /5
            print("alpha changed",i,alpha,w)
        else:
            wprev = w
        yt= x*w
        diff = (x*w-y)
        diff_sig = sigma(10,diff,x)
        w=w-(alpha*diff_sig/M)
        #print("Difference",w,diff_sig, alpha*diff_sig/M)
        lossprev = loss

    return w,i


w,i = GradientDescent(100,0.1)
print("Final",w,i)

ytilde = x*w
ytilderaw = np.squeeze(np.asarray(ytilde))
fig, ax = subplots()
ax.plot(xraw, yraw, 'bo')
ax.plot(xraw, ytilderaw, 'ro')
ax.set_xlabel("height")
ax.set_ylabel("weight")
show()
