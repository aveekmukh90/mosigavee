(deftemplate Category ; A category of text (e.g. scientific, legal, spam, etc) 
(slot NAME (type SYMBOL))
(slot M (type INTEGER)) )

(deftemplate WordPair ; structure for ccounting Word Pairs (2-Grams of words)
(slot CATEGORY (type SYMBOL))
(slot WORD1 (type SYMBOL))
(slot WORD2 (type SYMBOL))
(slot COUNT (type INTEGER)) ; Number of instances of word pair
)



(defglobal ?*cnt*  = 0)


(defrule CreateWordPairFact
    	(Paragraph ?class $? ?w1 ?w2 $?) 
=>
(bind ?*cnt* (+ ?*cnt* 1))
(assert (wordpair ?class ?w1 ?w2 ?*cnt*))
(printout t ?w1 " + " ?w2 crlf)
)

(defrule totalCountingDoneCheck
(forall (Paragraph ?class $? ?w1 ?w2 $?)
		(wordpair ?class ?w1 ?w2 ?))
=>
(assert(total-counting-done))
)

(defrule MakeWordPair
(wordpair ?class ?w1 ?w2 ?) 
=> (assert (WordPair (CATEGORY ?class) (WORD1 ?w1) (WORD2 ?w2) (COUNT 0))) 
	(assert (Category (NAME ?class) (M 0)))
)


(defrule CountWordPairs
?wp <- (wordpair ?class ?w1 ?w2 ?)
?word-fact <- (WordPair (CATEGORY ?class) (WORD1 ?w1) (WORD2 ?w2) (COUNT ?c) )
?cat-fact <- (Category (NAME ?class) (M ?m))
=>
(retract ?wp) 
(modify ?word-fact  (COUNT (+ ?c 1)))
(modify ?cat-fact  (M (+ ?m 1)))
)

(assert (Paragraph mail hello world hello world)))



(assert (Category (NAME ALL) (M 0)))


;One way of doing it! The trivial way!
(defrule CountPairsForAllCategories
?tcd <- (total-counting-done)
=>
(assert (Category (NAME All) (M ?*cnt*)))
(retract ?tcd)
)



;Other way of doing it! 

(defglobal ?*tot* = 0)

(defrule CountPairsForAllCategories
(forall (Category (M ?m))
		())
=>
(bind ?*tot* (+ ?*tot* ?m))
;(assert (Category (NAME All) (M ?*tot*)))
)

(assert (Category (NAME All) (M ?*tot*)))

;best way to do
(defrule CountPairsForAllCategories
   =>
   (bind ?tot 0)
   (do-for-all-facts ((?cat Category)) TRUE
   	(bind ?tot (+ ?tot ?cat:M))
    )
    (assert (Category (NAME All) (M ?tot)))
  )

;SOmething like this
(defrule MostFrequentWP
(WordPair (CATEGORY ?c) (WORD1 ?w1) (WORD2 ?w2) (COUNT ?c1))
(not (WordPair (CATEGORY ?c) (COUNT ?c2&:(> ?c2 ?c1))))
   =>
   (printout t "The most frequent word pair in category " ?c " is " ?w1 "," ?w2 " with count " ?c1 crlf)
)

