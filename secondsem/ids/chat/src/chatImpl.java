import java.rmi.RemoteException;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Objects;

public class chatImpl implements chat {
	LinkedList<User> activeUsers = new LinkedList<>();
	User defaultUser = new User("defaultUser", "defaultpass");
	
	@Override
	public synchronized boolean register(User newusr) throws RemoteException {
		User usr= findUser(newusr.userName);
		
		//If user does not exist add him
		if(usr == defaultUser){
			return activeUsers.add(newusr);
		}
		
		//if user exists validate password
		if(Objects.equals(usr.password,newusr.password)){
			return true;
		}
		
		throw new RemoteException("Invalid Password");
	}

	@Override
	public boolean send(String receiver, Message msg) throws RemoteException {
		//find the user from the list of active users and insert the message
		User usr= findUser(receiver);
		if(usr != defaultUser){
			return usr.newMessages.add(msg);
		}else{
			return false;
		}
	}

	@Override
	public LinkedList<Message> receive(User usr) throws RemoteException {
		//get the user
		User me = findUser(usr.userName);
		//get the users messages
		LinkedList<Message> msg = new LinkedList<Message>(me.newMessages);
		//clear the messages after user gets it
		me.newMessages.clear();
		return msg;
	}

	private User findUser(String userName){
		ListIterator<User> listIterator = activeUsers.listIterator();
        while (listIterator.hasNext()) {
        	User temp = listIterator.next();
            if(Objects.equals(temp.userName, userName)){
            	return temp;
            }
        }
        return defaultUser;
	}
	
	private void printActiveUsers(){
		ListIterator<User> listIterator = activeUsers.listIterator();
        while (listIterator.hasNext()) {
        	User temp = listIterator.next();
            System.out.println(temp.toString());
            }
        }
}

