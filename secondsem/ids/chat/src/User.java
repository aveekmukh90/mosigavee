import java.io.Serializable;
import java.util.*;;

public class User implements Serializable {
	String userName;
	String password;
	LinkedList<Message> newMessages = new LinkedList<>();
	
	public User(String usr , String pass){
		userName = usr;
		password = pass;
	}
	
	public String toString(){
     return "UserName:" + "\t" + userName;
  }
}
