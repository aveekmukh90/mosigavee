import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.LinkedList;
import java.util.ListIterator;

public class chatClient {
	public static void printAllMessages(LinkedList<Message> ls){
		ListIterator<Message> listIterator = ls.listIterator();
        while (listIterator.hasNext()) {
        	Message temp = listIterator.next();
            System.out.println(temp.toString());
            }
	}
	
	public static void main(String [] args){
	    try{
	      String host = "localhost";//args[0];

	      //Get remote object reference
	      Registry registry = LocateRegistry.getRegistry(host);
	      chat h = (chat) registry.lookup("ChatServer");

	      //register 1st user
	      User usr1 = new User("User1", "user1");
	      h.register(usr1);
	      
	      //register 2nd user
	      User usr2 = new User("User2" , "user2");
	      h.register(usr2);
	      
	      //Set 1
	      Message msg = new Message("User1" , "Hello you!");
	      h.send("User2", msg);
	      LinkedList<Message> messages = h.receive(usr2);
	      System.out.println("New message: "+messages.element().toString());
	     
	      Thread.sleep(2000);
	      
	      //set 2
	      msg = new Message("User1" , "Hello you 1!");
	      h.send("User2", msg);
	      msg = new Message("User1" , "Hello you 2!");
	      h.send("User2", msg);
	      msg = new Message("User1" , "Hello you 3!");
	      h.send("User2", msg);
	      messages = h.receive(usr2);
	      System.out.println("New Messages:");
	      printAllMessages(messages);
	      
	      Thread.sleep(2000);
	      
	      //set 3
	      msg = new Message("User1" , "Hello you 4!");
	      h.send("User2", msg);
	      msg = new Message("User1" , "Hello you 5!");
	      h.send("User2", msg);
	      msg = new Message("User1" , "Hello you 6!");
	      h.send("User2", msg);
	      messages = h.receive(usr2);
	      System.out.println("New Messages:");
	      printAllMessages(messages);
	      	      
	    } catch (Exception e) {
	      System.err.println("Error on client: " + e) ;
	  }
	}
}
