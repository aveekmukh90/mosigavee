import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.LinkedList;

public interface chat extends Remote{
	//register a new user to our chat server
	public boolean register(User usr)
	throws RemoteException;
	
	//send  message to the server
	public boolean send(String receiver, Message msg)
	throws RemoteException;
	
	//polls messages from the server
	public LinkedList<Message> receive(User usr)
	throws RemoteException;
}
