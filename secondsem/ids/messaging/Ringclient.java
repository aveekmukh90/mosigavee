import java.io.IOException;
import java.util.*;

import com.rabbitmq.client.*;

public class Ringclient implements Runnable{

	private String IN_QUEUE_NAME = "hello";
	private String OUT_QUEUE_NAME = "out";
	private String HOST = "localhost";
	private String NAME = "Node";
	
	static Channel inchannel;
	
	public Ringclient(String inQueue , String outQueue , String name, String host){
		IN_QUEUE_NAME = inQueue;
		OUT_QUEUE_NAME = outQueue;
		HOST = host;
		NAME = name;
	}
	
	public Ringclient(String inQueue , String outQueue , String name){
		IN_QUEUE_NAME = inQueue;
		OUT_QUEUE_NAME = outQueue;
		NAME = name;
	}
	
	public void init(){
		try{
			ConnectionFactory factory = new ConnectionFactory();
		    factory.setHost(HOST);
		    Connection connection = factory.newConnection();
		    inchannel = connection.createChannel();
		    inchannel.queueDeclare(IN_QUEUE_NAME, false, false, false, null);
		    System.out.println(" [*] "+ NAME + " Up!");
		    
		}catch(Exception e){
			
		}		
	}
	
	
	public void run(){	
		Consumer consumer = new DefaultConsumer(inchannel) {
		      @Override
		      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
		          throws IOException {
		    	//if message belongs to me , I'll process it
		  		//else i'll forward it
		    	String recipient = properties.getType();
		    	String message = new String(body, "UTF-8");
		    	//System.out.println(" [x] Received '" + message + "' for "+recipient);
		    	
		    	if(NAME.equals(recipient)){
			        System.out.println(NAME + " Received '" + message);
		    	}else{
			        inchannel.basicPublish("", OUT_QUEUE_NAME, properties , message.getBytes("UTF-8"));
			        System.out.println(" [x] "+ NAME + " Forwarded '" + message + "' for "+recipient);
		    	}  
		      }
		 };
		
		 try{
			 inchannel.basicConsume(IN_QUEUE_NAME, true, consumer);
		 }catch(Exception e){
			 
		 }
	}
	
	public void run(String recipient , String message){
		try{
			System.out.println(NAME + " sending '"+message+"' to "+recipient);
			inchannel.basicPublish("", OUT_QUEUE_NAME, new AMQP.BasicProperties.Builder().type(recipient).build() , message.getBytes("UTF-8"));
		}catch(Exception e){
			
		}
		run();
	}

	  public static void main(String[] argv) throws Exception {
		  Scanner scanner = new Scanner(System.in);

		  /*
		  System.out.println("Input: #nodes");
		  int nodes = scanner.nextInt();
		  System.out.println("Input: #Start node");
		  int startnode = scanner.nextInt();
		  System.out.println("Input: message");
		  String message = scanner.next();
		  */
		  
		  Ringclient rc1 = new Ringclient("in1","in2","Node1");
		  Ringclient rc2 = new Ringclient("in2","in3","Node2");
		  Ringclient rc3 = new Ringclient("in3","in4","Node3");
		  Ringclient rc4 = new Ringclient("in4","in1","Node4");
		  rc1.init();
		  rc2.init();
		  rc3.init();
		  rc4.init();
		  
		  rc1.run();
		  rc2.run();
		  rc3.run("Node2","hello");
		  rc4.run();
		  
	  }
}
