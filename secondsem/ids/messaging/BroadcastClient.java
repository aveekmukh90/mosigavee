import java.io.IOException;
import java.util.*;

import com.rabbitmq.client.*;

public class BroadcastClient {
	
	private static String EXCHANGE_NAME = "broadcastmessages";
	private static String HOST = "localhost"; 
	private Channel channel;
	private String name;
	private String queueName;
	
	public BroadcastClient(String name){
		this.name=name;
		this.queueName = name;
	}
	
	public void init(){
		try{
			ConnectionFactory factory = new ConnectionFactory();
	        factory.setHost("localhost");
	        Connection connection = factory.newConnection();
	        channel = connection.createChannel();
	        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
	        channel.queueDeclare(queueName, false, false, false, null);
	        channel.queueBind(queueName, EXCHANGE_NAME, "");
	        
	        System.out.println(name +" up and running!");
		}catch(Exception e){}
		
	}
	
	public void run(){

	    Consumer consumer = new DefaultConsumer(channel) {
	      @Override
	      public void handleDelivery(String consumerTag, Envelope envelope,
	                                 AMQP.BasicProperties properties, byte[] body) throws IOException {
	        String message = new String(body, "UTF-8");
	        System.out.println(name + " Received '" + message + "'");
	      }
	    };
	    
	    try{
	    	channel.basicConsume(queueName, true, consumer);
	    }catch(Exception e){}
	}
	
	public void run(String recipient , String message){
		
		try{
			channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes());
		}catch(Exception e){}
        //System.out.println(name +" Sent '" + message + "' to "+recipient);
        System.out.println(name +" Sent '" + message);
        
        run();
	}
	
    public static void main(String[] argv)
                  throws java.io.IOException {

    	BroadcastClient bc1 = new BroadcastClient("Node1");
    	bc1.init();
    	
    	BroadcastClient bc2 = new BroadcastClient("Node2");
    	bc2.init();
    	
    	BroadcastClient bc3 = new BroadcastClient("Node3");
    	bc3.init();
    	
    	BroadcastClient bc4 = new BroadcastClient("Node4");
    	bc4.init();
    	
    	bc1.run();
    	bc2.run();
    	bc3.run("Node4", "Bonjour!");
    	bc4.run();
    }
    
	
}
