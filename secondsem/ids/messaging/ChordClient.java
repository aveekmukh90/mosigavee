import java.io.IOException;
import java.util.*;

import com.rabbitmq.client.*;

public class ChordClient {
	
	private static String BROADCAST_EXCHANGE_NAME = "Query_P2P";
	private static String TOPIC_EXCHANGE_NAME = "Topic_P2P";
	private static String HOST = "localhost"; 
	private Channel channel;
	private String name;
	private String queueName;
	private HashMap<String,String> data;
	public boolean isRunning = false;
	
	public ChordClient(String name){
		this.name=name;
		this.queueName = name;
		this.data = new HashMap<String,String>();
	}
	
	public void init(){
		try{
			ConnectionFactory factory = new ConnectionFactory();
	        factory.setHost("localhost");
	        Connection connection = factory.newConnection();
	        channel = connection.createChannel();
	        channel.exchangeDeclare(BROADCAST_EXCHANGE_NAME, "fanout");
	        channel.queueDeclare(queueName, false, false, false, null);
	        channel.queueBind(queueName, BROADCAST_EXCHANGE_NAME, "");
	        
	        //Initialise the topic queue
	        channel.exchangeDeclare(TOPIC_EXCHANGE_NAME, "topic");
	        
	        System.out.println(name +" up and running!");
		}catch(Exception e){}
		
	}
	
	//In this method we try to understand what kind of message is this
	//Returns 0 if message is my answer , 1 if I am the sender of a 
	public int handleMessage(String msg){
		String[] parts = null;
		int ret_code = -1;
		String ret=null;
		
		if(msg.contains(":")){
			parts = msg.split(":");
		}else{
			return 0;
		}
		
		switch(parts[0]){
			case "find":
				//message querying something
				ret = data.get(parts[1]);
		}
		
		if(ret!=null){
			System.out.println(name + " found '" + parts[1]);// + "' Description ="+ret);
			ret_code = 1;
			//publish the message to listening clients
			try{
				channel.basicPublish(TOPIC_EXCHANGE_NAME, msg , null, ret.getBytes());
			}catch(Exception e){}
		}else{
			ret_code = -1;
		}
		
		return ret_code;
	}
	
	public void addData(String k, String v){
		data.put(k, v);
	}
	
	public void run(){
		isRunning = true;
		
	    Consumer consumer = new DefaultConsumer(channel) {
	      @Override
	      public void handleDelivery(String consumerTag, Envelope envelope,
	                                 AMQP.BasicProperties properties, byte[] body) throws IOException {
	        String message = new String(body, "UTF-8");
	        int ret = handleMessage(message);
	        if(ret == 0){
	        	System.out.println(name + " received "+message +" for "+envelope.getRoutingKey());
	        }
	        
	      }
	    };
	    
	    try{
	    	channel.basicConsume(queueName, true, consumer);
	    }catch(Exception e){}
	}
	
	public void run(String message){
		
		try{
			channel.basicPublish(BROADCAST_EXCHANGE_NAME, "", null, message.getBytes());
			//start listening for the requested message
			channel.queueBind(queueName, TOPIC_EXCHANGE_NAME, message);
			
		}catch(Exception e){}
        //System.out.println(name +" Sent '" + message + "' to "+recipient);
        System.out.println(name +" Sent '" + message+"'");
        
        run();
	}
	
	public static void menu(){
		Scanner reader = new Scanner(System.in);
		int numnodes = 0;
		int querynode = 0;
		String query = ""; 
		ChordClient[] clients = new ChordClient[100] ;
		
		System.out.println("Step 1: How many nodes do you want?");
		numnodes = reader.nextInt();
		
		for(int i=0;i<numnodes;i++){
			String nodename = "Node"+(i+1);
			System.out.println(nodename);
			clients[i] = new ChordClient(nodename);
			clients[i].init();
		}
		
		
		while(true){
			System.out.println("Step 2: Add data to nodes. Input 0 to skip or enter the node number:");
			int node = reader.nextInt();
			if(node == 0)
				break;
			System.out.println("Enter key:");
			String key = reader.next();
			System.out.println("Enter value:");
			reader.nextLine(); 
			String val = reader.nextLine();
			clients[node-1].addData(key, val);
		}	
		
		while(true){
			System.out.println("Step 3: Command a node to query a data. Input 0 to skip or enter the node number:");
			int node = reader.nextInt();
			if(node == 0)
				break;
			System.out.println("Enter query:");
			query = reader.next();
			query = "find:"+query;
			
			clients[node-1].run(query);
		}
		
		for(int i=0;i<numnodes;i++){
			if(!clients[i].isRunning)
				clients[i].run();
		}
		
	}
	
    public static void main(String[] argv)
                  throws java.io.IOException {
    	
    	ChordClient.menu();
    	return;
/*
    	ChordClient bc1 = new ChordClient("Node1");
    	bc1.init();
    	
    	ChordClient bc2 = new ChordClient("Node2");
    	bc2.init();
    	
    	ChordClient bc3 = new ChordClient("Node3");
    	bc3.init();
    	
    	ChordClient bc4 = new ChordClient("Node4");
    	bc4.init();
    	
    	bc1.run();
    	bc2.run();
    	bc1.addData("titanic", "Titanic is a 1997 movie by Kate Winslet and Leonardo Dicaprio.");
    	bc2.addData("intouchable", "Intouchables est un film français réalisé par Olivier Nakache et Éric Toledano, sorti en France le 2 novembre 2011 au cinéma.");
    	bc3.run("find:titanic");
    	bc4.run("find:intouchable");
    	*/
    }
    
	
}
