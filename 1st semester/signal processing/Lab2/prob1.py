# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 14:47:36 2015

@author: aveekmukherjee
"""

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 500,500)
x1 = np.sin(2*np.pi*x/100)
x2 = 4*np.exp(-1/300*(x-150)**2) - np.exp(-1/2500*(x-150)**2)
x3 = []
x4 = np.random.normal(0, 1 , 500)

def fun_x3(k):
    if k>= 240 and k <300:
        return 1;
    if k>=300 and k<380 :
        return -1
    return 0
    
for i in x:
    x3.append(fun_x3(i))
    

plt.figure()
plt.plot(x, x1 , color = 'red' , label = "x1")
plt.plot(x, x2 , color = "green" , label = "x2")
plt.plot(x, x3 , color = "blue" , label = "x3")
plt.plot(x, x4 , color = "pink" , label = "x4")
plt.legend()

plt.figure()
plt.plot(x, x1+x2+x3 , color = 'red' , label = "x1+x2+x3")
plt.legend()

plt.figure()
plt.plot(x, x1+x2+x3+x4 , color = 'red' , label = "x1+x2+x3+x4")
plt.legend()

#plotting the single low pass filter
def singleLowPassFilter(X):
    y=[0]
    for i in range(1,500):
        y.append((0.05*X[i] + 0.95*y[i-1]))
    return y
        
        
    
plt.figure()
plt.plot(x, singleLowPassFilter(x1) , color = 'red' , label = "x1")
plt.plot(x, singleLowPassFilter(x2) , color = "green" , label = "x2")
plt.plot(x, singleLowPassFilter(x3) , color = "blue" , label = "x3")
plt.plot(x, singleLowPassFilter(x4) , color = "pink" , label = "x4")
plt.legend()
plt.axis('tight')
plt.show()

plt.figure()
plt.plot(x, singleLowPassFilter(x1+x2+x3) , color = 'red' , label = "x1")
plt.legend()
plt.axis('tight')
plt.show()