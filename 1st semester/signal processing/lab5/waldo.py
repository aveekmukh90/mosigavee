# -*- coding: utf-8 -*-
"""
Created on Sun Dec 5 14:15:17 2015

@author: aveekmukherjee
"""

import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from scipy.fftpack import fft2,fftshift,ifft2
from scipy.signal import correlate2d
import re 

#image_file = "damierHV.pgm"
# Function to read a pgm image from a file
def read_pgm(filename, byteorder='>'):
    """Return image data from a raw PGM file as numpy array.
 
    Format specification: http://netpbm.sourceforge.net/doc/pgm.html
 
    """
    with open(filename, 'rb') as f:
        buffer = f.read()
    try:
        header, width, height, maxval = re.search(
            b"(^P5\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n]\s)*)", buffer).groups()
    except AttributeError:
        raise ValueError("Not a raw PGM file: '%s'" % filename)
    return np.frombuffer(buffer,
                            dtype='u1' if int(maxval) < 256 else byteorder+'u2',
                            count=int(width)*int(height),
                            offset=len(header)
                            ).reshape((int(height), int(width)))

image_file = "FindWaldo1.pgm"
waldo_file = "WadoTarget1.pgm"
image = read_pgm(image_file , plt.cm.gray)
waldo = read_pgm(waldo_file , plt.cm.gray)

waldo = waldo - waldo.mean()
plt.figure()                      
plt.imshow(image, plt.cm.gray)

plt.figure()                      
plt.imshow(waldo, plt.cm.gray)

corr = correlate2d(image , waldo, mode='same')

[y,x] = np.unravel_index(np.argmax(corr), corr.shape)

plt.figure()          
plt.plot(x, y, 'ro')            
plt.imshow(image, plt.cm.gray)


        
