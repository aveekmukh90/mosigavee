# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 23:07:28 2015

@author: aveekmukherjee
"""

import numpy as np 
import matplotlib.pyplot as plt 
from skimage import io 
from scipy.fftpack import fft2, fftshift, ifft2 
import math

from skimage import color, data
 
ima = data.lena() 
plt.figure()   
ima = color.rgb2gray(ima)  
plt.imshow(ima , plt.cm.gray)  
plt.title('Original image')  

Nx = ima.shape[0]
Ny = ima.shape[1]

def cylinder_filter(Nx, Ny, cutOffFrequency, typeFilter):
    xc = 0
    yc = 0
   
    a = int(cutOffFrequency * Nx)
    b = int(cutOffFrequency * Ny)
   
    x = np.linspace(0, Nx-1, Nx)  
    y = np.linspace(0, Ny-1, Ny)  
    x2DCoord, y2DCoord = np.meshgrid(x, y) 
          
    ellipse = np.zeros((Nx, Ny))
    xScale = (x2DCoord-xc)/a  
    yScale = (y2DCoord-yc)/b
       
    ellipse = (xScale)**2 + (yScale)**2
       
    if typeFilter == 'LP':  
        segmentEllipse = ellipse <= 1 #type boolean  
    else:  
        segmentEllipse = ellipse > 1 #type boolean  
      
    segmentEllipse = 1*segmentEllipse #type integer 
       
    return segmentEllipse
#Return a 2D array HF (Transfert function of the filter) with the size : 
#number of lines = Nx  
#number of columns = Ny  
#cut-off frequency in 1/pixel  
#typeFilter = 'LP' --> Low pass filter 
#typeFilter = 'HP' --> High pass filter
    
out = cylinder_filter(512 , 512 , 1/8 , 'LP' )

dimg = fft2(ima)

ima = ifft2(dimg*out)
ima = np.real(ima)

plt.figure() 
plt.imshow(ima, plt.cm.gray)  
plt.title('Filtered image')  