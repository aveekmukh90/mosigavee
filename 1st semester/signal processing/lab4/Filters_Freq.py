from skimage import color, data
import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft2, fftshift, ifft2
# from Frequency_Spatial_Scale import display_plot

SAVE_IMAGE = False

def display_plot(x, y, title="Plot", xlabel="x", ylabel="y", limits=[], legend=False):
    plt.figure()
    if isinstance(y, list):
        for i in range(len(y)):
            # plt.plot(x, y[i], lebel=str(i))
            plt.plot(x, y[i])
    else:
        plt.plot(x, y)
    plt.grid()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if legend:
        plt.legend()
    if len(limits) > 0:
        plt.axis(limits)
    # if SHOW_TITLE:
        # plt.title(title)
    if SAVE_IMAGE:
        plt.savefig('../output/' + title.replace(' ', '_') + '.png')
    plt.show()

def display_image(image, title="Image", vmin=None, vmax=None, extent=None):
    plt.imshow(image, plt.cm.gray, vmin=vmin, vmax=vmax, extent=extent)
    if SAVE_IMAGE:
        plt.savefig('../output/' + title.replace(' ', '_') + '.png')
    plt.show()

def cylinder_filter(Nx, Ny, cutOffFrequency, typeFilter):
    xc = 0
    yc = 0
   
    a = int(cutOffFrequency * Nx)
    b = int(cutOffFrequency * Ny)
   
    x = np.linspace(0, Nx-1, Nx)  
    y = np.linspace(0, Ny-1, Ny)  
    x2DCoord, y2DCoord = np.meshgrid(x, y) 
          
    ellipse = np.zeros((Nx, Ny))
    xScale = (x2DCoord-xc)/a  
    yScale = (y2DCoord-yc)/b
       
    ellipse = (xScale)**2 + (yScale)**2 
       
    if typeFilter == 'LP':  
        segmentEllipse = ellipse <= 1 #type boolean  
    else:  
        segmentEllipse = ellipse > 1 #type boolean  
      
    segmentEllipse = 1*segmentEllipse #type integer 
       
    return segmentEllipse

def butterworth_filter(Nx, Ny, cutOffFrequency, n):
    res = np.zeros((Nx, Ny))
    for x in range(Nx):
        for y in range(Ny):
            fx = 1. * x / Nx
            fy = 1. * y / Ny
            res[x, y] = 1. / (1 + (np.sqrt(fx ** 2 + fy ** 2) / cutOffFrequency) ** (2 * n))
    return res


def gaussian_filter(Nx, Ny, sigma_x, sigma_y):
    res = np.zeros((Nx, Ny))
    for x in range(Nx):
        for y in range(Ny):
            fx = 1. * x / Nx
            fy = 1. * y / Ny
            res[x, y] = np.exp(- 0.5 * (fx ** 2) / (sigma_x ** 2) - 0.5 * (fy ** 2) / (sigma_y ** 2))
    return res

ima_filtered = []
def apply_filter_and_show(ima_ft, myfilter):
    global ima_filtered
    ima_ft_filtered = ima_ft * myfilter
    ima_filtered = (ifft2(ima_ft_filtered)).real
    # ima_filtered_y0 = ima_ft_filtered[0]
    return ima_filtered


if __name__ == '__main__':
# Question 2.2
    ima = color.rgb2gray(data.lena())
    # display_image(ima)
    ima_ft = fft2(ima)

# Question 2.4 and 2.5

    cyl_filter = cylinder_filter(ima.shape[0], ima.shape[1], 1. / 16, 'LP')
    plt.figure()
    plt.imshow(cyl_filter)
    cyl_ima = apply_filter_and_show(ima_ft, cyl_filter)
    display_image(cyl_ima , "Cylinder filter")

# Question 2.6
    butt_filter = butterworth_filter(ima.shape[0], ima.shape[1], 1. / 16, 4)
    butt_ima = apply_filter_and_show(ima_ft, butt_filter)
    display_image(butt_ima , "Butterworth filter")

# Question 2.7
    sigma = 1. / 16 / np.sqrt(2 * np.log(2))
    gaus_filter = gaussian_filter(ima.shape[0], ima.shape[1], sigma, sigma)
    gauss_ima = apply_filter_and_show(ima_ft, gaus_filter)
    display_image(gauss_ima , "Gaussian filter")

    print (ima.shape)
    print (cyl_ima.shape)
    print (butt_ima.shape)
    print (gauss_ima.shape)

    display_plot(range(len(cyl_ima[0])), [ima[0], cyl_ima[0], butt_ima[0], gauss_ima[0]], legend=True)




