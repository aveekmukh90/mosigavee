# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 15:56:08 2015

@author: aveekmukherjee
"""
import numpy as np
import matplotlib.pyplot as plt

def generateDelta(IdxSampleWhereImpuls , numberOfSamples):
    x=[]
    for i in range(0,numberOfSamples):
        if(i in IdxSampleWhereImpuls):
            x.append(1)
        else:
            x.append(0)
    return x
    
print (generateDelta([1],20));

