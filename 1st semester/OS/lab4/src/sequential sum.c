#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 2000000000

static unsigned long long num_arr[SIZE];

typedef struct thread_struct{
	int start;
	int end;
	unsigned long long sum;
}ts;

unsigned long long sum_array(unsigned long long *arr, int start, int end){
	unsigned long long sum = 0;
	int i =0;
	for( i =start; i<= end; i++){
			sum = sum + num_arr[i];
		}
		
	return sum;
}

static void * thread_sum(void * tsv)
{
	ts * tsp = (ts*)tsv; 
	tsp->sum = sum_array(num_arr,tsp->start, tsp->end-1);
	//printf("Adding from %d to %d and sum = %llu\n" , tsp->start, tsp->end-1 , tsp->sum );
	return NULL;
}

int main (int argc, char **argv){
	clock_t start = clock();
	
	unsigned long long sum = 0;
	int i =0;
	
	//Initialize array
	for( i =0; i< SIZE; i++){
		num_arr[i] = i+1;
	}
	
	//Sequential addition
	#ifdef SEQ
		sum = sum_array(num_arr , 0, SIZE-1);
	#endif
	
	#ifndef SEQ
	int max_threads = 1;
	pthread_t *tids ;
	ts* threadstructs;
	
	if(argc > 1){
		max_threads = atoi(argv[1]);
	}
	
	tids = malloc (max_threads*sizeof(pthread_t)) ;
	threadstructs = malloc(max_threads*sizeof(ts));
	
	int block_size = SIZE/max_threads;
	int remainder = SIZE%max_threads;
	
	/* Create the threads for sum */
	for (i = 0 ; i < max_threads; i++){
		threadstructs[i].start = i*block_size;
		threadstructs[i].end = threadstructs[i].start + block_size;
		
		pthread_create (&tids[i], NULL, thread_sum, (void *)&threadstructs[i] ) ;
	}
	
	if(remainder){
		sum = sum_array(num_arr , SIZE-remainder, SIZE-1);
	}
	  
	/* Wait until every thread ended */ 
    for (i = 0; i < max_threads; i++){
		pthread_join (tids[i], NULL) ;
    }
    
    for(i =0 ; i< max_threads ; i++)
    {
		sum +=  threadstructs[i].sum;
	}
	  
	#endif
	
	printf("Sum of array = %llu\n" , sum);
	
	printf("time taken %lf\n", (double)(clock() - start));
}
