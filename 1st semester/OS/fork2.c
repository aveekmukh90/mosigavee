#include "stdio.h"

void main()
{
   int f = 0;

   printf("Starting\n");

   f= fork();

   if(f==0)
   {
	printf("I am child %d\n", getpid());	
	execl("/bin/ls","ls", NULL);
   }else{
	printf("I am parent %d\n", getpid());
   }
}
