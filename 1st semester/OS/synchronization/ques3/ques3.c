#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

#define RUNWAYS 2

void* land(void * dummy);


typedef struct node{
	struct node *  next;
	pthread_cond_t cond;
	int flight_land_number;
}node;

typedef struct queue{
	node * first;
	node * last;
	int count;
}queue;

void push_into_queue(queue *q,node *n)
{
	q->count++;
	n->next = NULL;
	n->flight_land_number = q->count;

	printf("Log: pushed to queue: flight: %d\n",n->flight_land_number);
	if(q->first == NULL){
		//we have an empty queue
		q->first = n;
		q->last = n;
		return;
	}

	q->last->next = n;
	q->last =n;

	return;
}

//Responsibility to free memory is with caller
node * pop_from_queue(queue *q)
{
	if (q->first == NULL){
		//We have an empty list
		return NULL;
	}
	q->count--;
	node * n = q->first;
	q->first = q->first->next;

	if(q->first == NULL){
		//Our queue is empty now
		q->last = NULL;
	}

	printf("Log: popped from queue: flight %d\n",n->flight_land_number);
	return n;
}

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t runway = PTHREAD_COND_INITIALIZER;
int runway_free=RUNWAYS;
queue * qnormalplanes;

void main(int argc , char ** argv)
{
	pthread_mutex_init(&mutex, NULL);
	int max_flights = 1;
	int i =0;

	//Initialize the queue
	qnormalplanes = (queue *)malloc(sizeof(queue));
	qnormalplanes->first = NULL;
	qnormalplanes->last = NULL;
	qnormalplanes->count = 0;


	if(argc > 1){
		max_flights = atoi(argv[1]);
	}

	pthread_t *tids = malloc (max_flights*sizeof(pthread_t));

	for (i = 0 ; i < max_flights; i++){
		pthread_create (&tids[i], NULL, land, NULL ) ;
	}

	/* Wait until every thread ended */
	  for (i = 0; i < max_flights; i++){
		pthread_join (tids[i], NULL) ;
	  }
}

void* land(void * dummy)
{
	//I should allow to land only if no one using the runway
	pthread_mutex_lock(&mutex);
	while(runway_free == 0){
		node * n;
		n = (node*)malloc(sizeof(node));
		n->cond = (pthread_cond_t)PTHREAD_COND_INITIALIZER;
		n->next = NULL;
		push_into_queue(qnormalplanes,n);
		printf("All runways busy!! waiting\n");
		pthread_cond_wait(&n->cond , &mutex);
	}

	runway_free -= 1;
	pthread_mutex_unlock(&mutex);

	//I am here means I have access to the runway
	//Let's pretend that I am using the runway by sleeping
	printf("Landing!! %ld\n", time(NULL));
	sleep(5);

	//Now that i have used the runway I need to notify that I am done using the runway
	pthread_mutex_lock(&mutex);
	runway_free += 1;
	if(qnormalplanes->count){
		node* n = pop_from_queue(qnormalplanes);
		pthread_cond_signal(&n->cond);
		//Do not leak memory
		free(n);
	}
	pthread_mutex_unlock(&mutex);

	return NULL;
}
