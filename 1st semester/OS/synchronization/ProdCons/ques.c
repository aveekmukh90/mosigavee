#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAXBUFFERLEN 10

void* producer(void*);
void* consumer(void*);

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t can_be_consumed = PTHREAD_COND_INITIALIZER;
pthread_cond_t can_be_produced = PTHREAD_COND_INITIALIZER;

typedef struct node{
	struct node *  next;
	int data;
}node;

typedef struct buffer{
	node * first;
	node * last;
	int count;
}buffer;

void push_into_buffer(buffer *b , node * n)
{
	b->count++;
	n->next = NULL;

	//printf("Log: pushed to buffer\n");
	if(b->first == NULL){
		//we have an empty buffer
		b->first = n;
		b->last = n;
		return;
	}

	b->last->next = n;
	b->last =n;

	return;
}

node * pop_from_buffer(buffer *b)
{
	if (b->first == NULL){
		//We have an empty list
		return NULL;
	}
	b->count--;
	node * n = b->first;
	b->first = b->first->next;

	if(b->first == NULL){
		//Our buffer is empty now
		b->last = NULL;
	}

	//printf("Log: popped from buffer\n");
	return n;
}

buffer * sharedbuffer;

void main(int argc , char ** argv)
{
	int num_threads_p = 0 , i =0 , num_threads_c = 0;

	//Initialize a global shared buffer
	sharedbuffer = (buffer*)malloc(sizeof(buffer));
	sharedbuffer->count=0;
	sharedbuffer->first=NULL;
	sharedbuffer->last =NULL;

	if(argc > 1){
		num_threads_p = atoi(argv[1]);
		num_threads_c = atoi(argv[2]);
	}

	pthread_t *tids_p = malloc (num_threads_p*sizeof(pthread_t));

	for (i = 0 ; i < num_threads_p; i++){
		pthread_create (&tids_p[i], NULL, producer, NULL ) ;
	}
	pthread_t *tids_c = malloc (num_threads_c*sizeof(pthread_t));

	for (i = 0 ; i < num_threads_c; i++){
		pthread_create (&tids_p[i], NULL, consumer, NULL ) ;
	}

	/* Wait until every producer thread ended */
	  for (i = 0; i < num_threads_p; i++){
		pthread_join (tids_p[i], NULL) ;
	  }

   /* Wait until every consumer thread ended */
	  for (i = 0; i < num_threads_c; i++){
		 pthread_join (tids_c[i], NULL) ;
	  }

}

void* producer(void* dummy)
{
	//Let me produce by taking some time
	sleep(rand()%5);
	node *n = (node *)malloc(sizeof(node));
	n->data = time(NULL);
	printf("I produced my data : %d\n" , n->data);

	pthread_mutex_lock(&mutex);
	if(sharedbuffer->count == MAXBUFFERLEN){
		//We should not produce more, I will wait till somebody consumes
		printf("I produced my data and I am blocked : %d\n" , n->data);
		pthread_cond_wait(&can_be_produced , &mutex);
	}

	//I am here means I should add my produce to the buffer
	push_into_buffer(sharedbuffer , n);
	printf("I added my data : %d\n" , n->data);

	//I should also condition a consumer
	pthread_cond_signal(&can_be_consumed);
	pthread_mutex_unlock(&mutex);

	return NULL;
}

void* consumer(void* dummy)
{
	node * n = NULL;
	pthread_mutex_lock(&mutex);
	if(sharedbuffer->count == 0){
		//We should not consume more, I will wait till somebody consumes
		printf("consumer blocked\n");
		pthread_cond_wait(&can_be_consumed , &mutex);
	}

	n = pop_from_buffer(sharedbuffer);
	//I should also condition a producer
	pthread_cond_signal(&can_be_produced);
	pthread_mutex_unlock(&mutex);

	//Now I can do whatever I want with the data
	printf("I consumed data : %d\n" , n->data);
}
