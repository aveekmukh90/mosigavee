#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define RUNWAYS 2

void* land(void * dummy);


typedef struct node{
	struct node *  next;
	pthread_cond_t cond;
	int flight_land_number;
}node;

typedef struct queue{
	node * first;
	node * last;
	int count;
	char name[10];
}queue;

void push_into_queue(queue *q,node *n)
{
	q->count++;
	n->next = NULL;
	n->flight_land_number = q->count;

	printf("Log: pushed to queue %s: flight: %d\n", q->name, n->flight_land_number);
	if(q->first == NULL){
		//we have an empty queue
		q->first = n;
		q->last = n;
		return;
	}

	q->last->next = n;
	q->last =n;

	return;
}

//Responsibility to free memory is with the caller
node * pop_from_queue(queue *q)
{
	if (q->first == NULL){
		//We have an empty list
		return NULL;
	}
	q->count--;
	node * n = q->first;
	q->first = q->first->next;

	if(q->first == NULL){
		//Our queue is empty now
		q->last = NULL;
	}

	printf("Log: popped from queue %s: flight %d\n",q->name, n->flight_land_number);
	return n;
}

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t runway = PTHREAD_COND_INITIALIZER;
int runway_free=RUNWAYS;
queue * qnormalplanes;
queue * qministerplanes;

void main(int argc , char ** argv)
{
	pthread_mutex_init(&mutex, NULL);
	int max_flights = 1;
	int i =0;
	int normal_priority = 0;
	int high_priority = 1;

	//Initialize the normal queue
	qnormalplanes = (queue *)malloc(sizeof(queue));
	qnormalplanes->first = NULL;
	qnormalplanes->last = NULL;
	qnormalplanes->count = 0;
	strcpy(qnormalplanes->name , "Normal");

	//Initialize the minister queue
	qministerplanes = (queue *)malloc(sizeof(queue));
	qministerplanes->first = NULL;
	qministerplanes->last = NULL;
	qministerplanes->count = 0;
	strcpy(qministerplanes->name , "Minister");


	if(argc > 1){
		max_flights = atoi(argv[1]);
	}

	pthread_t *tids_n = malloc (max_flights*sizeof(pthread_t));

	for (i = 0 ; i < max_flights; i++){
		pthread_create (&tids_n[i], NULL, land, (void*)&normal_priority ) ;
	}
	pthread_t *tids_m = malloc (max_flights*sizeof(pthread_t));

	for (i = 0 ; i < max_flights; i++){
		pthread_create (&tids_m[i], NULL, land, (void*)&high_priority ) ;
	}

	/* Wait until every thread ended */
	  for (i = 0; i < max_flights; i++){
		pthread_join (tids_n[i], NULL) ;
		pthread_join (tids_m[i], NULL) ;
	  }
}

void* land(void * priority)
{
	//I should allow to land only if no one using the runway
	pthread_mutex_lock(&mutex);
	while(runway_free == 0){
		node * n;
		n = (node*)malloc(sizeof(node));
		n->cond = (pthread_cond_t)PTHREAD_COND_INITIALIZER;
		n->next = NULL;

		if(*(int*)(priority) == 1){
			push_into_queue(qministerplanes,n);
		}else{
			push_into_queue(qnormalplanes,n);
		}

		printf("All runways busy!! waiting\n");
		pthread_cond_wait(&n->cond , &mutex);
	}

	runway_free -= 1;
	pthread_mutex_unlock(&mutex);

	//I am here means I have access to the runway
	//Let's pretend that I am using the runway by sleeping
	printf("Landing!! %ld\n", time(NULL));
	sleep(5);

	//Now that i have used the runway I need to notify that I am done using the runway
	pthread_mutex_lock(&mutex);
	runway_free += 1;

	queue * q = NULL;
	if(qnormalplanes->count > 0){
		q = qnormalplanes;
	}
	if(qministerplanes->count > 0){
		q = qministerplanes;
	}
	if(q != NULL){
		node* n = pop_from_queue(q);
		pthread_cond_signal(&n->cond);
		//Do not leak memory
		free(n);
	}

	pthread_mutex_unlock(&mutex);

	return NULL;
}
