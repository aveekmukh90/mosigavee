#include "mem_alloc.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>

/* memory */
char memory[MEMORY_SIZE]; 

/* Pointer to the first free block in the memory */
free_block_t first_free; 


#define ULONG(x)((long unsigned int)(x))
#define max(x,y) (x>y?x:y)

void is_all_free(void){
	if(first_free->size != MEMORY_SIZE){
		printf("There are memory leaks in your program\n");
	}
}

void sanity_check(char * addr)
{
	if(addr > memory + MEMORY_SIZE  || addr < memory)
		exit(1);
}

void analyze_free_blocks(void){
	free_block_t temp = first_free;
	int max = 0;
	int total_free = 0;
	while(temp){
		if(temp->size > max)
			max = temp->size;
		total_free += temp->size;
		temp=temp->next;
	}
	printf("Total_free\t%d\tMax_free\t%d\n",total_free,max);
}

void memory_init(void){
  /* 
   * Lets initialize a memory
   * Initially all blocks will be free
   * We need to store the book keeping info
   * */

   //The first free block is the start of the memory
   first_free = (free_block_t)memory;
   first_free->next = NULL;
   first_free->size = MEMORY_SIZE;

   //atexit(is_all_free);
}

int find_free_block(free_block_t * temp_free_p , free_block_t * prev_free_p , int effective_size , int strategy)
{
	int free_block_found = 0;
	free_block_t prev_free = NULL;
	free_block_t temp_free = first_free;
	free_block_t dummy_prev_free;
	free_block_t dummy_temp_free;
	int max_waste = -1;
	int min_waste = MEMORY_SIZE;

	switch(strategy){
	case STRATEGY_FIRST_FIT:
		while(temp_free){
			/*
			 * Implementing first free approach : If I find a free block big enough to accommodate actual size,
			 * I am good to go
			 */
			if(temp_free->size >= effective_size){
				/*
				 * I found a free block
				 */
				free_block_found =1;
				break;
			}

			prev_free = temp_free;
			temp_free = temp_free->next;
			//sanity_check((char*)temp_free->next);
		}
		break;
	case STRATEGY_BEST_FIT:
		min_waste = MEMORY_SIZE;
		dummy_prev_free = prev_free;
		dummy_temp_free = temp_free;
		while(dummy_temp_free){
			/*
			 * Implementing best free approach : If I find a free block big enough to accommodate actual size,
			 * I am good to go
			 */
			if(dummy_temp_free->size >= effective_size){
				/*
				 * I found a free block but i need to find if this is the best
				 */
				free_block_found =1;
				if((dummy_temp_free->size - effective_size) < min_waste) {
					temp_free = dummy_temp_free;
					prev_free = dummy_prev_free;
					min_waste = dummy_temp_free->size - effective_size;

					printf("I was best %d\n", min_waste);
				}
			}

			dummy_prev_free = dummy_temp_free;
			dummy_temp_free = dummy_temp_free->next;
		}
		break;
	case STRATEGY_WORST_FIT:
		max_waste = -1;
		dummy_prev_free = prev_free;
		dummy_temp_free = temp_free;
		while(dummy_temp_free){
			/*
			 * Implementing best free approach : If I find a free block big enough to accommodate actual size,
			 * I am good to go
			 */
			if(dummy_temp_free->size >= effective_size){
				/*
				 * I found a free block but i need to find if this is the worst
				 */
				free_block_found =1;
				if((dummy_temp_free->size - effective_size) > max_waste) {
					temp_free = dummy_temp_free;
					prev_free = dummy_prev_free;
					max_waste = dummy_temp_free->size - effective_size;

					printf("I was worst %d\n", max_waste);
				}
			}

			dummy_prev_free = dummy_temp_free;
			dummy_temp_free = dummy_temp_free->next;
		}
		break;
	}


	//Set the out parameters
	*temp_free_p = temp_free;
	*prev_free_p = prev_free;

	return free_block_found;
}

char *memory_alloc(int size){
	//Effectively i need to have a block that can store my book keeping information as well hence size + Size_Busy_Block
	// but once a busy block becomes free, I need to store the free info there, hence the minimum is Size_Free_Block
	//int effective_size = size + Size_Busy_Block < Size_Free_Block ? Size_Free_Block : size + Size_Busy_Block ;
	int effective_size = size + Size_Free_Block ;
	int free_block_found = 0;
	char * return_addr;
	free_block_t prev_free = NULL;
	free_block_t temp_free = first_free;
	
	free_block_found = find_free_block(&temp_free , &prev_free , effective_size, STRATEGY);

	if( !free_block_found || !temp_free){
		//We could not find a free block, so return NULL
		 return_addr = NULL;
		 return return_addr;
	}

	//We are here means we found a free block, now we need to handle it
	//Can we break the free block into another free block?
	//Do we at least have a space to create a free block with capacity 1??
	int new_block_size = temp_free->size - effective_size - sizeof(free_block_s);
	busy_block_s bb;
	if( new_block_size >= 1){
		//If yes, allocate from the front, i.e keep the free_block book keeping in the same position in memory
		temp_free->size = temp_free->size - effective_size;
		//The new address for busy block is
		char *addr = (char*)temp_free + temp_free->size ;
		//Now allocate the busy block

		bb.size = effective_size;
		//copy this busy block book keeping into the buffer
		memcpy((void *)addr,(void *)&bb,sizeof(bb));

		//return the address to the caller
		return_addr = addr+sizeof(bb);

	}else{
		//We cannot break the free block into another free block
		bb.size = temp_free->size;

		//Assign the next free block as next of the previous free block
		if(prev_free)
			prev_free->next = temp_free->next;

		//If we are dealing with the first block??
		if(temp_free == first_free)
		{
			first_free = first_free->next;
		}

		memcpy((void *)temp_free,(void *)&bb,sizeof(bb));
		return_addr = (char*)temp_free + sizeof(bb);
	}

	//printf("Size_requested\t%d\tactual_allocated\t%d\n", size , bb.size);
	//print_alloc_info(addr, actual_size);

	return return_addr;
}

//Todo: Improve on the logic to Check if the value passed is actually supposed to be free
void memory_free(char *p){
	if(p==NULL){
		//Fail safe
		return;
	}

  //print_free_info(p);

  //find out the book keeping, the book keeping was stored right before the returned memory to user
  //So, lets go back and find it out
  busy_block_t bb = (busy_block_t)(p - Size_Busy_Block);

  if(bb->size <= 0 || bb->size > MEMORY_SIZE){
	  //looks like illegal free
	  //printf("Looks like illegal free: %d. Ignoring.\n", bb->size);
	  return;
  }

  //find the free block immediately before this
  free_block_t temp_free = first_free;
  free_block_t prev_free = NULL;

  if(first_free == NULL)
  {
	  //we are dealing with the first block
	  first_free=(free_block_t)bb;
	  //We can do this as we stored the effective size in the busy block's size info
	  first_free->size=bb->size;
	  first_free->next = NULL;
	  
	  //And we are done
	  return;
  }
  
  //We are here means , we already have a free block
  //We should find where is the first free block,
  //It could be before the busy block or after it
  //Let's find out
  int is_after=0;
  if((char*)first_free > (char*)bb){
	  is_after = 1;
  }

  if(is_after){
	  //Busy block is before the first free block
	  prev_free = (free_block_t)bb;
	  prev_free->size = bb->size;
	  prev_free->next = first_free;
	  first_free = prev_free;
  }else{
	  //Busy block is after the first free block
	  //So , now we will find the free block that was just before the block we are supposed to free
	  while((char*)temp_free < (char*)bb && temp_free != NULL)
	  {
		  prev_free = temp_free;
		  temp_free = temp_free->next;
	  }

	  //so now prev_free points to the free node just before the one we want to free
	  if((char*)bb == ((char*)prev_free + prev_free->size))
	  {
		  //if bb is right next to prev_free, merge them
		  prev_free->size += bb->size;
	  }else{
		  //it is not right next, so just keep a track
		  free_block_s ff;
		  ff.size = bb->size;
		  memcpy((void *)bb ,(void *)&ff, sizeof(ff));

		  free_block_t fp = (free_block_t)bb;
		  fp->next = prev_free->next;
		  prev_free->next = fp;

	  }
  }

  //If the next block is also free, merge it too , the logic takes care of NULL pointer exception as well
  if((char*)prev_free + prev_free->size == (char*)prev_free->next ){
	  prev_free->size += prev_free->next->size;

	  if(prev_free->next != NULL){
		  prev_free->next = prev_free->next->next;
	  }
  }

}


void print_info(void) {
  fprintf(stderr, "Memory : [%lu %lu] (%lu bytes)\n", (long unsigned int) 0, (long unsigned int) (memory+MEMORY_SIZE), (long unsigned int) (MEMORY_SIZE));
  fprintf(stderr, "Free block : %lu bytes; busy block : %lu bytes.\n", ULONG(sizeof(free_block_s)), ULONG(sizeof(busy_block_s))); 
}

void print_free_info(char *addr){
  if(addr)
    fprintf(stderr, "FREE  at : %lu \n", ULONG(addr - memory)); 
  else
    fprintf(stderr, "FREE  at : %lu \n", ULONG(0)); 
}

void print_alloc_info(char *addr, int size){
  if(addr){
    fprintf(stderr, "ALLOC at : %lu (%d byte(s))\n", 
	    ULONG(addr - memory), size);
  }
  else{
    fprintf(stderr, "Warning, system is out of memory\n"); 
  }
}

void print_free_blocks(void) {
  free_block_t current; 
  fprintf(stderr, "Begin of free block list :\n"); 
  for(current = first_free; current != NULL; current = current->next)
    fprintf(stderr, "Free block at address %lu, size %u\n", ULONG((char*)current - memory), current->size);
}

char *heap_base(void) {
  return memory;
}


void *malloc(size_t size){
  static int init_flag = 0; 
  if(!init_flag){
    init_flag = 1; 
    memory_init(); 
    //print_info();
  }

  /*
  static int count = 50;
  	if(count == 50){
  		count = 0;
  		analyze_free_blocks();
  	}else{
  		count++;
  	}
  	*/
  return (void*)memory_alloc((size_t)size); 
}

void free(void *p){
  if (p == NULL) return;
  memory_free((char*)p); 
  //print_free_blocks();
}

void *realloc(void *ptr, size_t size){
  if(ptr == NULL)
    return memory_alloc(size); 
  busy_block_t bb = ((busy_block_t)ptr) - 1; 
  //printf("Reallocating %d bytes to %d\n", bb->size - (int)sizeof(busy_block_s), (int)size);
  if(size <= bb->size - sizeof(busy_block_s))
    return ptr; 

  char *new = memory_alloc(size); 
  memcpy((void *)new, (void*)(bb+1), bb->size - sizeof(busy_block_s) ); 
  memory_free((char*)(bb+1)); 
  return (void*)(new); 
}

void print_busy_info(char *addr){
	busy_block_t bb = (busy_block_t)(addr-sizeof(busy_block_s));
	printf("busy size : %d\n" , bb->size );
}

#ifdef MAIN
int main(int argc, char **argv){

  /* The main can be changed, it is *not* involved in tests */
  memory_init();
  print_info(); 
  print_free_blocks();

  int i ; 
  for( i = 0; i < 10; i++){
    char *b = memory_alloc(rand()%8);
    memory_free(b); 
    print_free_blocks();
  }


  char * a = memory_alloc(15);
  a=realloc(a, 20); 
  memory_free(a);


  a = memory_alloc(10);
  memory_free(a);

  printf("%lu\n",(long unsigned int) (memory_alloc(9)));

  return EXIT_SUCCESS;
}
#endif 
