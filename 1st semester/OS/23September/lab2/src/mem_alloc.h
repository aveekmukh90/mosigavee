#ifndef   	_MEM_ALLOC_H_
#define   	_MEM_ALLOC_H_

#ifndef MEMORY_SIZE
	#define MEMORY_SIZE 2048
#endif

#define STRATEGY STRATEGY_BEST_FIT
#define STRATEGY_FIRST_FIT 0
#define STRATEGY_BEST_FIT 1
#define STRATEGY_WORST_FIT 2

#define Size_Free_Block sizeof(free_block_s)
#define Size_Busy_Block sizeof(busy_block_s)

/* Structure declaration for a free block */
typedef struct free_block{
  int size;
  struct free_block *next;
} free_block_s, *free_block_t;

/* Structure declaration for an occupied block */
typedef struct busy_block{
  int size;
} busy_block_s, *busy_block_t;


/* Allocator functions, to be implemented in mem_alloc.c */
void memory_init(void);
char *memory_alloc(int size); 
void memory_free(char *p);

//Finds the free block based on strategy
int find_free_block(free_block_t * temp_free , free_block_t * prev_free , int effective_size , int strategy);
/*Finds which blocks have not been freed*/
void is_all_free(void);
//analyze fragmentation
void analyze_free_blocks(void);
//sanity check an address
void sanity_check(char * addr);

/* Logging functions */
void print_info(void); 
void print_alloc_info(char *addr, int size); 
void print_free_info(char *addr); 
void print_free_blocks(void); 
char *heap_base(void); 

#include <stdlib.h>
//void *malloc(size_t size);
//void free(void *p);
//void *realloc(void *ptr, size_t size);

void print_busy_info(char *addr);

#endif 	    /* !_MEM_ALLOC_H_ */
