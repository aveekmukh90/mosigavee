# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 14:20:45 2015

@author: aveekmukherjee
"""

from threading import Thread, Lock
import time;

mutex = Lock()
is_wc_available = True

def enterWC():
    global is_wc_available
    while(is_wc_available == False):
        print("I am waiting for WC")
    mutex.acquire()
    is_wc_available = False
    print("I acquired WC");
    mutex.release()
    
def exitWC():
    global is_wc_available
    mutex.acquire()
    is_wc_available = True
    print("I released WC");
    mutex.release()

def processData(data):
    print("Thread trying for WC" + str(data))
    enterWC()
    # doing non critical stuff
    exitWC()

c=0
while c<10:
    t = Thread(target = processData, args = (c,))
    c=c+1
    t.start()