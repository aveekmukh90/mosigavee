/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //app.receivedEvent('deviceready');
        var dev = document.getElementById("device");
        dev.appendChild(document.createTextNode(device.cordova + ";" + device.model+ ";" +device.platform + ";"+device.isVirtual));

        navigator.globalization.getPreferredLanguage(
            function (language) {
              var glob = document.getElementById("global");
              glob.appendChild(document.createTextNode("Language: "+language.value));},
            function () {alert('Error getting language\n');}
        );

        function isScreenReaderRunningCallback(boolean) {
            var glob = document.getElementById("access");
            if (boolean) {
                glob.appendChild(document.createTextNode("Screen reader: ON"));
                // Do something to improve the behavior of the application while a screen reader is active.
            } else {
                glob.appendChild(document.createTextNode("Screen reader: OFF"));
            }
        }
        MobileAccessibility.isScreenReaderRunning(isScreenReaderRunningCallback);


        function onSuccess(acceleration) {
          var glob = document.getElementById("accel");
          glob.innerHTML = 'Acceleration X: ' + acceleration.x + '\n' +
                  'Acceleration Y: ' + acceleration.y + '\n' +
                  'Acceleration Z: ' + acceleration.z + '\n' +
                  'Timestamp: '      + acceleration.timestamp + '\n';
        }

        function onError() {
            alert('Hardware Error!');
        }

        var options = { frequency: 3000 };  // Update every 3 seconds

        var watchID = navigator.accelerometer.watchAcceleration(onSuccess, onError, options);

        var successHandler = function (pedometerData) {
          var glob = document.getElementById("pedo");
            glob.appendChild(document.createTextNode(pedometerData.distance));
            // pedometerData.startDate; -> ms since 1970
            // pedometerData.endDate; -> ms since 1970
            // pedometerData.numberOfSteps;
            // pedometerData.distance;
            // pedometerData.floorsAscended;
            // pedometerData.floorsDescended;
        };
        pedometer.startPedometerUpdates(successHandler, onError);

    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();
