(function() {
  'use strict';

  angular.module('application', [
    'ui.router',
    'ngAnimate',

    //foundation
    'foundation',
    'foundation.dynamicRouting',
    'foundation.dynamicRouting.animations'
  ])
    .config(config)
    .run(run)
    .controller('FoodController',function($scope, $state, $stateParams, $http , $controller){
      angular.extend(this, $controller('DefaultController', {$scope: $scope, $stateParams: $stateParams, $state: $state}));
      $scope.categories =[
        {'name':'seasonal' , 'color' : '#a5dff9'},
        {'name':'american' , 'color' : '#a8dba8'},
        {'name':'continental' , 'color' : '#79bd9a'},
        {'name':'indian' , 'color' : '#ef5285'},
        {'name':'french' , 'color' : '#9DC8C8'},
        {'name':'british' , 'color' : '#58C9B9'},
        {'name':'german' , 'color' : '#D1B6E1'}];
        sessionStorage.categories=JSON.stringify($scope.categories);
    })
  ;

  config.$inject = ['$urlRouterProvider', '$locationProvider'];

  function config($urlProvider, $locationProvider) {
    $urlProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled:false,
      requireBase: false
    });

    $locationProvider.hashPrefix('!');
  }

  function run() {
    FastClick.attach(document.body);
  }

})();
